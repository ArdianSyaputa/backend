package com.crud.react.jwt;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.TemporaryToken;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.TemporaryTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private  static String secretKey = "belajar Spring";
    private static Long expired = 900000L;
@Autowired
private TemporaryTokenRepository temporaryTokenRepository;

@Autowired
private UsersRepository usersRepository;

    public  String generateToken(UserDetails userDetails){
      String token = UUID.randomUUID().toString().replace("-","");
        Users users = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("UserNot"));
        var chekingToken = temporaryTokenRepository.findByUserId(users.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken= new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(users.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token){
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token Eror parse"));
    }

    public Boolean checkingTokenJwt (String token){
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null){
            System.out.println("Token Kosong");
            return false;
        }

        if (tokenExist.getExpiredDate().before(new Date())){
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}
